# Nostalgie Radio Picker & Metadata Fetching Script

[[_TOC_]]

## Live Demo

The demo is on youtube: https://youtu.be/rXta7-3vpNA

[![](https://markdown-videos.deta.dev/youtube/rXta7-3vpNA)](https://youtu.be/rXta7-3vpNA)

## The `radio` Script

`radio` is a python script that displays the list of Nostalgie radio stations (see https://www.nostalgie.fr/) and allow you to search and pick one to play with mpv.

### Installation
First install the dependencies:
```
dnf install qt fzf
pip install --user pyfzf==0.3.1
pip install --user requests==2.28.1
```

* `qdbus` (part of `qt`) is required to update the tab title of Konsole once a radio station has been picked:  
  ![terminal-title-station.png](images/terminal-title-station.png "Konsole tab")
* `fzf` is used to select the radio station,
* `pyfzf` is a python package that offers bindings to fzf in python,
* `requests` is a python package to retrieve web pages.

### Usage

```
usage: radio [-h] [-u] [-n] [FILTER]

Nostalgie radio station picker and player.

positional arguments:
  FILTER         Filter for fzf

options:
  -h, --help     show this help message and exit
  -u, --update   Update the list of radio stations
  -n, --no-term  Do not change the terminal title
```

#### Update The List of Stations

First retrieve or update the list of streams:
```
radio --update
```
![radio-update.png](images/radio-update.png "Uptaging the radio list")

A hash of both the current and the new files is displayed, along with the number of radio stations in each. This provides visual feedback whether the list has been updated with new stations.

The list of radio stations is stored in `~/.radios.nostalgie.json`.

Tip: I have this command run on a daily basis in a user cron job.

#### List All Sations

Then invoke the script:
```
radio
```
The fzf selector/picker will show up:

![radio-pick.png](images/radio-pick.png "fzf picker")

#### List a Subset of All Stations

It's also possible to provide the script with a filter string on the cli:
```
radio 80
```
Will give a list of the radios that match the filter:

![radio-pick-80.png](images/radio-pick-80.png "radio 80")

#### Navigation

Use the arrow keys (&uarr; and &darr;) to move the cursor inside the list.

Press `Escape` or `Ctrl`+`C` to leave.

Tip: `Ctrl`+`R` will pick a station at random in the current list (see below)

Type anything else to specify a filter (read about fzf to learn how it works)

#### Pick a Station At Random

Simply use `Ctrl`+`R` to pick at random one of the stations in the list currently held by fzf.

#### Play a Station

Validate the current selection with the `Enter` key.

`mpv` (the media player) will then be spawned to play the audio stream for the radio station that has been selected:

![mpv-playing.png](images/mpv-playing.png "mpv playing the selected radio station")

## Metadata Fetching Script (for `mpv`)

Modern audio streaming services based on shoutcast/icecast have the ability to send metadata with the audio stream. Typically a string of text that contains the title of the song and the name of the artist. These updates are sent at regular intervals during the course of a song.

Nostalgie does send these out-of-band metadata but they have decided to rather send a numeric code that identifies a song rather than a string containing the song title and name of the artist being streamed. This is because the audio streams are meant to be used from their website.

Fortunately an endpoint exists that will allow us to convert these codes into song title and artist name: https://www.nostalgie.fr/onair

The shell script `nostalgie-art-title.run` is automatically launched by mpv. It listens to these out-of-band events sent by the icecast server and performs the translation by querying said endpoint. Despite multiple identical updates being sent during the streaming of a song, the script outputs the song details once only.

### Installation

Simply copy the script `nostalgie-art-title.run` under: `~/.config/mpv/scripts/` (you may need to create the directory beforehand) and make sure it is executable (`chmod 0600`)

The script depends on `jq` because the http endpoint content is json formatted.

### Usage

Just start mpv on a stream from one of the Nostalgie radio stations.

The song title and artist name will be displayed as part of `mpv` output (with the prefix `nostalgie:`):

![mpv-title-artist.png](images/mpv-title-artist.png "mpv showing title and artist")

The terminal title will also be updated (works for Konsole and its derivatives like Yakuake):

![terminal-tab-title.png](images/terminal-tab-title.png "Konsole tab")

However when the environment variable `RADIO_NO_TERM` is set (non-empty), the script won't change the terminal title. This is automatically set by `radio` when it is given the `--no-term` option on its cli.

### Integration with the `radio` script

The `radio` script presented at the top of this document will set an environment variable name `RADIO` to the name of the radio so that the script can display it when it starts.
